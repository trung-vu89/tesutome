class SiteController < ApplicationController
  before_filter :authenticate_user!, except: [:about, :contact]

  def index
    @projects = current_user.projects.sort_by{ |e| e[:updated_at] }
    @projects = @projects.reverse
    @invitation_messages = ProjectMember.where(:user_id => current_user.id, :status => ProjectMember::REQUESTING)
    render :index
  end

  def about
    render :about
  end

  def contact
    render :contact
  end
end
