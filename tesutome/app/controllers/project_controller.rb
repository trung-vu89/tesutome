class ProjectController < ApplicationController
  def create
    if params[:title].present?
      project = Project.create(title: params[:title], description: params[:description], icon_url: params[:icon_url], user_id: current_user.id)
      redirect_to :action => 'view', :id => project.id
    else
      @project = Project.new
      render :new
    end
  end

  def view
    @project = Project.find(params[:id])
    render :view
  end

  def search_user
    current_project = Project.find(params[:project_id])
    project_members = current_project.members
    project_invitation_pending_members = current_project.invitation_pending_members.pluck(:user_id)
    all_users = User.where("email like ?", "%#{params[:name]}%").all
    @search_users = []
    all_users.each do |user|
      unless project_members.include?(user) || project_invitation_pending_members.include?(user.id)
        @search_users << user
      end
    end
    @current_project_id = params[:project_id]
    respond_to do |format|
      format.html
      format.js {render layout: false}
    end
  end

  def invite
    project_id = params[:project_id]
    user_id = params[:user_id]
    role = params[:role]
    result = false
    project_member = ProjectMember.where(:project_id => project_id, :user_id => user_id).all.first
    if project_member.present?
      if project_member.status == ProjectMember::REQUESTING || project_member.status == ProjectMember::ACCEPTED
        result = false
      else
        project_member.update_attributes(:status => ProjectMember::REQUESTING, :role => role)
        project_member.save!
        result = true
      end
    else
      project_member = ProjectMember.create(:project_id => project_id, :user_id => user_id, :role => role, :status => ProjectMember::REQUESTING)
      result = true
    end
    respond_to do |format|
      format.html
      format.json {
        render json: {
          :status => result,
          :invitation_pending_member => {
            :id => project_member.id,
            :project_id => project_member.project_id,
            :user_name => project_member.user.user_name,
            :role_name => project_member.role_name
          }
        }
      }
    end
  end

  def invite_cancel
    respond_to do |format|
      format.html
      format.json { render json: {:status => invitation_process(params[:invitation_id], ProjectMember::CANCELED)}}
    end
  end

  def invite_accept
    respond_to do |format|
      format.html
      format.json { render json: {:status => invitation_process(params[:invitation_id], ProjectMember::ACCEPTED)}}
    end
  end

  def invite_reject
    respond_to do |format|
      format.html
      format.json { render json: {:status => invitation_process(params[:invitation_id], ProjectMember::REJECTED)}}
    end
  end

  private

  def invitation_process(invitation_id, status)
    return false if invitation_id.blank? || status.blank?
    invitation = ProjectMember.find(params[:invitation_id])
    if invitation.present? && invitation.status == ProjectMember::REQUESTING
      invitation.update_attribute(:status, status)
      invitation.save!
      return true
    end
    return false
  end
end
