class ImageController < ApplicationController

  require 'net/http'
  require 'net/https'
  require 'open-uri'
  require 'json'
  require 'base64'

  API_URI = URI.parse('https://api.imgur.com/3/')
  API_PUBLIC_KEY = 'Client-ID cdafb7066b5dcd1'

  ENDPOINTS = {
    :image => 'image',
  }

  def upload
    result = nil
    img = File.open(params[:image].tempfile, "rb") {|io| io.read}
    params = {
      :image => Base64.encode64(img)
    }
    request = Net::HTTP::Post.new(API_URI.request_uri + ENDPOINTS[:image])
    request.set_form_data(params)
    request.add_field('Authorization', API_PUBLIC_KEY)
    response = web_client.request(request)
    result = JSON.parse(response.body) #['data']['link']
    respond_to do |format|
      format.html
      format.json { render json: {:status => result.present? ? result['success'] : false, :image_url => result.present? ? result['data']['link'] : nil}}
    end
  end

  private

  def web_client
    http = Net::HTTP.new(API_URI.host, API_URI.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    http
  end
end
