class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :project_members

  def user_name
    return self.email.split('@').first
  end

  def projects
    result = Project.all.where(user_id: self.id)
    self.project_members.each do |project_member|
      result << project_member.project if project_member.status == ProjectMember::ACCEPTED
    end
    return result
  end
end
