class ProjectMember < ActiveRecord::Base
  belongs_to :user
  belongs_to :project

  validates :user_id, :project_id, :role, :status, numericality: {only_integer: true}, allow_nil: false

  ROLE_IDS = {
    (DEVELOPER = 10) => "Developer",
    (TESTER    = 20) => "Tester",
    (WATCHER   = 30) => "Watcher"
  }

  STATUS_IDS = {
    (REQUESTING = 10) => "Invitation requesting",
    (ACCEPTED   = 20) => "Invitation accepted",
    (CANCELED   = 30) => "Invitation canceled",
    (REJECTED   = 40) => "Invitation rejected"
  }

  def role_name
    ROLE_IDS[self.role]
  end

end
