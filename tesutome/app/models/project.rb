class Project < ActiveRecord::Base
  belongs_to :user

  has_many :project_members

  validates :user_id, numericality: {only_integer: true}, allow_nil: false
  validates :title, uniqueness: true, allow_nil: false

  def owner
    return self.user
  end

  def members
    all_members = []
    all_members << self.owner
    project_members = self.project_members.where(status: ProjectMember::ACCEPTED).all
    if project_members.present? && project_members.count > 0
      project_members.each do |project_member|
        all_members << project_member.user
      end
    end
    return all_members
  end

  def invitation_pending_members
    return self.project_members.where(status: ProjectMember::REQUESTING).all
  end
end
