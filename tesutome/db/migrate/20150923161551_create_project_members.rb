class CreateProjectMembers < ActiveRecord::Migration
  def up
    create_table :project_members do |t|
      t.references :user, null: false
      t.references :project, null: false
      t.integer :role, null: false, default: 0
      t.integer :status, null: false, default: 0
      t.timestamps
      t.index [:user_id], name: "index_user_in_project_member"
      t.index [:project_id], name: "index_project_in_project_member"
    end
  end

  def down
    drop_table :project_members
  end
end
