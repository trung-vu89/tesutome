class CreateProjects < ActiveRecord::Migration
  def up
    create_table :projects do |t|
      t.string :title, null: false
      t.text :description
      t.string :icon_url
      t.references :user, null: false
      t.timestamps

      t.index [:title], unique: true, name: "index_project_title_unique"
      t.index [:user_id], name: "index_project_owner_id"
    end
  end
  def down
    drop_table :projects
  end
end
