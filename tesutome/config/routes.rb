Rails.application.routes.draw do
  devise_for :users
  root to: 'site#index'

  # Site controller
  get 'site/about' => 'site#about'
  match 'site/contact' => 'site#contact', via: [:get, :post]

  # Project controller
  match 'project/create' => 'project#create', via: [:get, :post]
  get 'project/:id/view' => 'project#view'
  get 'project/search/user/:project_id/:name' => 'project#search_user'
  post 'project/invite' => 'project#invite'
  put 'project/invite/cancel' => 'project#invite_cancel'
  put 'project/invite/accept' => 'project#invite_accept'
  put 'project/invite/reject' => 'project#invite_reject'

  # Image controller
  post 'image/upload' => 'image#upload'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
